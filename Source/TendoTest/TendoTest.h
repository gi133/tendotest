// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#ifndef __TENDOTEST_H__
#define __TENDOTEST_H__

// Hack to get the thing to compile the pch because of VS2015 Update 3 change.
#pragma warning(disable : 4599)

#include "EngineMinimal.h"
#include "EngineGlobals.h"
#include "Engine/Engine.h"

DECLARE_LOG_CATEGORY_EXTERN(CriticalErrors, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(Errors, Log, All);

#define STENCIL_ITEMHIGHLIGHT 255;

#endif