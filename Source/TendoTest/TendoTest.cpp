// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "TendoTest.h"


IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TendoTest, "TendoTest" );
DEFINE_LOG_CATEGORY(CriticalErrors);
DEFINE_LOG_CATEGORY(Errors);