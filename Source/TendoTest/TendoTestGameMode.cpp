// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "TendoTest.h"
#include "TendoTestGameMode.h"
#include "TendoTestCharacter.h"

ATendoTestGameMode::ATendoTestGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
