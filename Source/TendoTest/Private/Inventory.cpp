// Fill out your copyright notice in the Description page of Project Settings.

#include "TendoTest.h"
#include "Inventory.h"
#include "TendoTestCharacter.h"


// Sets default values for this component's properties
UInventory::UInventory()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	// Set the default inventory slot number and update the array to match.
	inventorySlotNumber = 6;
	UpdateInventorySlots();

	currentEquipedSlot = -1;
}


// Called when the game starts
void UInventory::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

// Called every frame
void UInventory::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	// ...
}

void UInventory::SetInventorySlots(const uint32 newSlotNumber)
{
	inventorySlotNumber = newSlotNumber;
	UpdateInventorySlots();
}

void UInventory::PickUpItem(const AItem* itemToPickup)
{
	// Find an empty slot. If there are none, simply exit and maybe throw out a warning.
	int32 emptySlot = GetEmptySlotIndex();

	if (emptySlot == -1)
	{
		// Do anything else you need to warn player there's no empty slot.
		InventoryFullWarning();
		return;
	}

	// Add the item to the empty slot.
	AddItemToInventorySlot(itemToPickup, emptySlot);
}

int32 UInventory::GetEmptySlotIndex()
{
	for (int8 i = 0; i < inventorySlotArray.Max(); i++)
		if (!inventorySlotArray[i].IsValid())
			return i;

	return -1;
}

bool UInventory::EquipItemInSlot(const int32 slot)
{
	if (currentItem.IsValid())
	{
		// DO something when there's already an item equipped.
		UnequipItem();
	}

	if (inventorySlotArray[slot].IsValid())
	{
		currentItem = inventorySlotArray[slot];

		currentEquipedSlot = slot;

		AttachItemToPlayer(currentItem.Get());

		currentItem.Get()->SetActorHiddenInGame(false);
		currentItem.Get()->Equip();
		return true;
	}
	
	return false;
}

bool UInventory::EquipItemInSlotSocket(const int32 slot, const FName socketName)
{
	if (currentItem.IsValid())
	{
		// DO something when there's already an item equipped.
		UnequipItem();
	}

	if (inventorySlotArray[slot].IsValid())
	{
		currentItem = inventorySlotArray[slot];

		currentEquipedSlot = slot;

		AttachItemToPlayer(currentItem.Get());

		currentItem.Get()->SetActorHiddenInGame(false);
		currentItem.Get()->Equip();
		return true;
	}

	return false;
}

bool UInventory::UnequipItem()
{
	if (currentItem.IsValid())
	{
		currentItem.Get()->Unequip();

		currentItem.Get()->SetActorHiddenInGame(true);
		//currentItem.Get()->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		//currentItem.Get()->SetActorLocation(FVector::ZeroVector);

		currentItem.Reset();

		currentEquipedSlot = -1;

		return true;
	}
	return false;
}

void UInventory::UpdateInventorySlots()
{
	inventorySlotArray.SetNumZeroed(inventorySlotNumber, true);
}

bool UInventory::isEmpty(int32 slotNumber)
{
	return !inventorySlotArray[slotNumber].IsValid();
}

void UInventory::InventoryFullWarning_Implementation()
{
	// DO something.
    GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "Inventory full.");
}

void UInventory::AttachItemToPlayer(const AItem* item)
{
	TWeakObjectPtr<AItem> itemPtr = item;

	// Check if item needs to be attached to hand socket.
	if (itemPtr.Get()->GetAttachParentActor() == NULL)
	{
		itemPtr.Get()->AttachToComponent
		(
			Cast<ATendoTestCharacter>(GetOwner())->GetMesh(),
			FAttachmentTransformRules::FAttachmentTransformRules
			(
				EAttachmentRule::SnapToTarget,
				true
			),
			"hand_rSocket"
		);
	}
}

void UInventory::AttachItemToPlayerSocket(const AItem* item, const FName socketName)
{
	TWeakObjectPtr<AItem> itemPtr = item;

	// Check if item needs to be attached to hand socket.
	if (itemPtr.Get()->GetAttachParentActor() == NULL)
	{
		itemPtr.Get()->AttachToComponent
		(
			Cast<ATendoTestCharacter>(GetOwner())->GetMesh(),
			FAttachmentTransformRules::FAttachmentTransformRules
			(
				EAttachmentRule::SnapToTarget,
				true
			),
			socketName
		);
	}
}

void UInventory::AddItemToInventorySlot(const AItem* itemToAdd, const int32 slot)
{
	inventorySlotArray[slot] = itemToAdd;

	AttachItemToPlayer(itemToAdd);

	inventorySlotArray[slot].Get()->Pickup();
}

bool UInventory::UseCurrentEquipedItem()
{
	if (!currentItem.IsValid())
		return false;

	currentItem.Get()->Use();

	return true;
}

bool UInventory::DropItemAtSlot(const int32 slot)
{
	// Check if slot exists.
	if (!isSlotNumberValid(slot))
	{
		UE_LOG(Errors, Error, TEXT("Slot %d not a valid slot number!"), slot);
		return false;
	}

	if (!inventorySlotArray[slot].IsValid())
	{
		UE_LOG(Errors, Error, TEXT("Tried to drop an item calling a slot that is already empty."));
		return false;
	}
		
	TWeakObjectPtr<AItem> itemToDrop;

	// Check if slot is currently equipped, if yes then remove all hooks to it.
	if (slot == currentEquipedSlot)
	{
		currentEquipedSlot = -1;
		itemToDrop = currentItem;
		currentItem.Reset();
	}
	else
		itemToDrop = inventorySlotArray[slot];

	itemToDrop.Get()->Unequip();
	itemToDrop.Get()->PrepareToDrop();
	itemToDrop.Get()->Drop();

	// Reset slot.
	inventorySlotArray[slot].Reset();

	// Drop the item.
	return true;
}

bool UInventory::isSlotNumberValid(const int32 slot)
{
	if ((slot > -1) && (slot < static_cast<int32>(inventorySlotNumber)))
		return true;
	return false;
}

int32 UInventory::GetCurrentEquipedSlot()
{
	return static_cast<int32>(currentEquipedSlot);
}

AItem* UInventory::GetItemAtSlot(int32 slot)
{
	if (isSlotNumberValid(slot))
		return inventorySlotArray[slot].Get();
	else
		return NULL;
}

bool UInventory::FireCurrentItem()
{
	if (currentItem.Get() != nullptr)
	{
		currentItem.Get()->Fire();
		return true;
	}
	return false;
}
