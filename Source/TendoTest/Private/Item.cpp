// Fill out your copyright notice in the Description page of Project Settings.

#include "TendoTest.h"
#include "../Public/Item.h"


// Sets default values
AItem::AItem()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	itemName = "";
}

// Called when the game starts or when spawned
void AItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AItem::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

FString AItem::GetItemTypeAsString(EItemType EnumValue)
{
	TWeakObjectPtr<UEnum> enumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EItemType"), true);
	
	if (!enumPtr.IsValid())
		return FString("Invalid enum value.");

	return enumPtr.Get()->GetEnumName(static_cast<int32>(EnumValue));
}

void AItem::Drop_Implementation()
{
	UE_LOG(LogTemp, Error, TEXT("Called Drop() on the base item class!"));
}

void AItem::Use_Implementation()
{
	UE_LOG(LogTemp, Error, TEXT("Called Use() on the base item class!"));
}

void AItem::Examine_Implementation()
{
	UE_LOG(LogTemp, Error, TEXT("Called Examine() on the base item class!"));
}

void AItem::Equip_Implementation()
{
	UE_LOG(LogTemp, Error, TEXT("Called Equip() on the base item class!"));
}

void AItem::Unequip_Implementation()
{
	UE_LOG(LogTemp, Error, TEXT("Called Unequip() on the base item class!"));
}

void AItem::PrepareToDrop_Implementation()
{
	UE_LOG(LogTemp, Error, TEXT("Called PrepareToDrop() on the base item class!"));
}

void AItem::onCanPickup_Implementation()
{
	UE_LOG(LogTemp, Error, TEXT("Called onCanPickup() on the base item class!"));
}

void AItem::Pickup_Implementation()
{
	UE_LOG(LogTemp, Error, TEXT("Called Pickup() on the base item class!"));
}

void AItem::Fire_Implementation()
{
	UE_LOG(LogTemp, Error, TEXT("Called Fire() on the base item class!"));
}

void AItem::onCantPickup_Implementation()
{
	UE_LOG(LogTemp, Error, TEXT("Called onCantPickup() on the base item class!"));
}