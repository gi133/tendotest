// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "TendoTestGameMode.generated.h"

UCLASS(minimalapi)
class ATendoTestGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ATendoTestGameMode();
};



