// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Item.generated.h"

UENUM(BlueprintType)
enum class EItemType : uint8
{
	IE_BaseItem		UMETA(DisplayName = "Base Item"),
	IE_Flashlight	UMETA(DisplayName = "Flashlight"),
	IE_Bottle		UMETA(DisplayName = "Bottle"),
};

UCLASS(Abstract)
class TENDOTEST_API AItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItem();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(Category = ItemData, BlueprintReadWrite, EditAnywhere)
	EItemType itemType;

	UFUNCTION(Category = ItemData, BlueprintCallable)
	FString GetItemTypeAsString(EItemType EnumValue);

	UPROPERTY(Category = ItemData, BlueprintReadWrite, EditAnywhere)
	FString itemName;

	UPROPERTY(Category = ItemData, BlueprintReadWrite, EditAnywhere)
	TWeakObjectPtr<UTexture> itemInventoryImage;

#pragma region Item Actions
	UFUNCTION(Category = ItemUsage, BlueprintCallable, BlueprintNativeEvent)
	void Drop();
	virtual void Drop_Implementation();

	UFUNCTION(Category = ItemUsage, BlueprintCallable, BlueprintNativeEvent)
	void Use();
	virtual void Use_Implementation();

	UFUNCTION(Category = ItemUsage, BlueprintCallable, BlueprintNativeEvent)
	void Examine();
	virtual void Examine_Implementation();

	UFUNCTION(Category = ItemUsage, BlueprintCallable, BlueprintNativeEvent)
	void Equip();
	virtual void Equip_Implementation();

	UFUNCTION(Category = ItemUsage, BlueprintCallable, BlueprintNativeEvent)
	void Unequip();
	virtual void Unequip_Implementation();

	UFUNCTION(Category = ItemUsage, BlueprintCallable, BlueprintNativeEvent)
	void PrepareToDrop();
	virtual void PrepareToDrop_Implementation();

	UFUNCTION(Category = ItemUsage, BlueprintCallable, BlueprintNativeEvent)
	void onCanPickup();
	virtual void onCanPickup_Implementation();

	UFUNCTION(Category = ItemUsage, BlueprintCallable, BlueprintNativeEvent)
	void onCantPickup();
	virtual void onCantPickup_Implementation();

	UFUNCTION(Category = ItemUsage, BlueprintCallable, BlueprintNativeEvent)
	void Pickup();
	virtual void Pickup_Implementation();

	UFUNCTION(Category = ItemUsage, BlueprintCallable, BlueprintNativeEvent)
	void Fire();
	virtual void Fire_Implementation();
#pragma endregion Item Actions
};
