// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "Item.h"
#include "Inventory.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TENDOTEST_API UInventory : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventory();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UFUNCTION()
	void SetInventorySlots(const uint32 newSlotNumber);

	UFUNCTION(Category = ItemManipulation, BlueprintCallable)
	void PickUpItem(const AItem* itemToPickup);

	UFUNCTION(Category = InventoryCheck, BlueprintCallable)
	int32 GetEmptySlotIndex();

	UFUNCTION(Category = ItemManipulation, BlueprintCallable)
	bool EquipItemInSlot(const int32 slot);

	UFUNCTION(Category = ItemManipulation, BlueprintCallable)
	bool EquipItemInSlotSocket(const int32 slot, const FName socketName);

	UFUNCTION(Category = ItemManipulation, BlueprintCallable)
	bool UnequipItem();

	UFUNCTION(Category = ItemManipulation, BlueprintCallable)
	void AddItemToInventorySlot(const AItem* itemToAdd, const int32 slot);

	UFUNCTION(Category = ItemManipulation, BlueprintCallable)
	bool UseCurrentEquipedItem();

	UFUNCTION(Category = ItemManipulation, BlueprintCallable)
	bool DropItemAtSlot(const int32 slot);

	UFUNCTION(Category = InventoryCheck, BlueprintCallable)
	bool isSlotNumberValid(const int32 slot);

	UFUNCTION(Category = InventoryCheck, BlueprintCallable)
	int32 GetCurrentEquipedSlot();

	UFUNCTION(Category = ItemManupulation, BlueprintCallable)
	AItem* GetItemAtSlot(int32 inventorySlotNumber);

	UFUNCTION(Category = ItemManipulation, BlueprintCallable)
	bool FireCurrentItem();

private:
	UPROPERTY()
	TArray<TWeakObjectPtr<AItem>> inventorySlotArray;

	UPROPERTY()
	uint32 inventorySlotNumber;

	UPROPERTY()
	uint32 currentEquipedSlot;

	UFUNCTION()
	void UpdateInventorySlots();

	UFUNCTION()
	bool isEmpty(int32 slotNumber);

protected:
	UFUNCTION(Category = InventoryWarning, BlueprintCallable, BlueprintImplementableEvent)
	void InventoryFullWarning();
	void InventoryFullWarning_Implementation();

	UFUNCTION(Category = InventoryManagement, BlueprintCallable)
	void AttachItemToPlayer(const AItem* item);

	UFUNCTION(Category = InventoryManagement, BlueprintCallable)
	void AttachItemToPlayerSocket(const AItem* item, const FName socketName);

	UPROPERTY(BlueprintReadOnly)
	TWeakObjectPtr<AItem> currentItem;
};
